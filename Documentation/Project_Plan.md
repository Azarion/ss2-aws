# Project Plan Special Subject 2 - Amazon Web Services

	Author	: Tim Nikolajsen
	Date	: 27/9/18
	Purpose	: Describe how to fullfil the tasks and learning goals of Special Subject 2 - Amazon Web Services
	Audience: Project Members

	
## Revision

| Revision  | Date       | Comments                                      							| Changes By  	 |
| --------- | ---------- | ------------------------------------------------------------------------	| -------------- |
| 1.0       | 27/09/2018 | Initial Document                              							| Tim Nikolajsen |


## Product

The product of this is to create a report which describes how to:

	- Document a project or process
	- Learn new skills and/or gain new knowledge
	- Explain technical or other relevant material within a given context
	- Present technical or other relevant material within a given context
	- Do structured project management

	

**Create a document where you outline how you will fullfill the 5 SS learning goals. Remember to consider
how you document the different parts.**