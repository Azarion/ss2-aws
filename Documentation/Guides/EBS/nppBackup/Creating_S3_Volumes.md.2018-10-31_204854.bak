# Elastic Block Store (EBS) 

	Author	: Tim Nikolajsen
	Date	: 2018-10-11
	Purpose	: Keep a guide to launch EBS Volumes.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-04 | Initial Document                              | Tim Nikolajsen |

___

***The all happens under 'Service', 'EC2', 'ELASTIC BLOCK STORE' Volumes and Snapshots.***

# Create EBS Volume

## Linux

- Use 'Launch Instance'
	- Select 'Amazon Linux 2 AMI (HVM)'.
	- Select 't2.micro' (Free Tier)
	- 'Review and launch' and edit the security group.
		- Pick a name for the security group.
		- Choose type SSH from the dropdown menu and set source to Anywhere. (Shouldn't be done in production environments)
		- Launch Instance
			- You can here choose to use a key pair for security or have Amazon create one.
			
- Use prefered way to ssh into the instance by copy/pasting the public IPv4(or IPv6) in the list of instances under instances to the left. (Instance username: ec2-user)


Notes about importing your own key pairs can be found [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#how-to-generate-your-own-key-and-import-it-to-aws)

## Windows Server

- Same procedure as above with 3 major differences.
	- Pick a Windows Server AMI instead
	- Pick RDP under security group.
	- Use prefered RDP software. (Windows 10 has this built-in) (Password for login must be decrypted with you key-pair)


# Install WordPress on Linux
- Follow Amazon's own [guide](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html) (Their own step-by-step guide)

Additional notes about can be found [here](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_40_EC2.md)