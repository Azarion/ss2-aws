# Elastic Block Store (EBS) 

	Author	: Tim Nikolajsen
	Date	: 2018-10-11
	Purpose	: Keep a guide to launch EBS Volumes.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-04 | Initial Document                              | Tim Nikolajsen |

___

***The all happens under 'Service', 'EC2', 'ELASTIC BLOCK STORE' Volumes and Snapshots.***

# EBS Volume

## Creating / Deleting
- Under Volumes
	- Create Volume
		- Pick Type (General Purpose SSD) and Size (Less than 10GB). (For free Tier)
		- Select the same availability zone as the EC2 instance is located in.
		- Choose whether or not to use encryption. (It is very transparant)
		- Create Volume
		
- Delete Volume
	- Select 'Delete Volume' in the context menu of the newly-made volume.


Additional [notes](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_41_EBS.md#ebs-notes)