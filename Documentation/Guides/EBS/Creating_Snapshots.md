# Elastic Block Store (EBS) 

	Author	: Tim Nikolajsen
	Date	: 2018-10-11
	Purpose	: Keep a guide to create and delete snapshots.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-11 | Initial Document                              | Tim Nikolajsen |

___

***The all happens under 'Service', 'EC2', 'ELASTIC BLOCK STORE' Snapshots.***


# Snapshots

## Creating Snapshots
- Select 'Create Snapshot' in the context menu of the volume.
	- Select a memorable name for the snapshot

## Recreate Volumes
- Under Snapshots in the menu to the left.
	- Select 'Create Volume' in the context menu of the Snapshot you made.
	

Additional [notes](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_41_EBS.md#ebs-snapshots)