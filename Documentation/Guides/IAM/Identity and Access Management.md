# Identity and Access Management

	Author	: Tim Nikolajsen
	Date	: 2018-09-27
	Purpose	: Keep a guide to change specified IAM settings.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-09-27 | Initial Document                              | Tim Nikolajsen |

___

# Create AWS account
- This part is straight forward with some points of note:
	- You have to use credit card information to make an account.
	- You have to call a bot to verify your account.

	
Further notes can be found [here](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_39_Account_Creation.md)

# Setup Account Security
- When logged in as root you create an IAM group in the 'Services' dropdown menu at the top of the screen and choose 'IAM' under 'Security, Identity & Compliance'.

### Create user and setup IAM Group with permissions policies.
- Here you will see a big grey box in the middle of the window called 'Security Status'. This helps with the creation of an IAM group and general account security.
	- To create an IAM group - open the second option in the Security Status and click 'Manage users'
	- From here click 'Add user' at the top and enter a user name and pick the appropriate access type after which you can either create you own password or have Amazon create one.
	- Next choose permissions by creating the IAM group and attach policies to it and afterwards add the user to the group.

### Create Access Key
- From the IAM menu click 'Manage User Access Keys' under 'Rotate User Access Keys'
	- From this menu open the tab called 'Security credentials' and use the 'Create access key' to receive your access key along with the 'Access Key ID'.