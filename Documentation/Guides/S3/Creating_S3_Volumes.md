# Simple Storage Service

	Author	: Tim Nikolajsen
	Date	: 2018-10-11
	Purpose	: Keep a guide to launch EBS Volumes.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-04 | Initial Document                              | Tim Nikolajsen |

___

***The all happens under 'Service', 'STOREGE', 'S3'.***

# Buckets

## Creating Buckets
- Click 'Create bucket'
	- Pick a name (must be globally unique)
	- Pick a Region
	- Next until you have created the Bucket

## Versioning
- Enabled by going into the properties of the Bucket and selecting Versioning.
- To show version of an object
	- Select 'Show' under Versions in the overview.


Additional [notes](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_41_EBS.md#ebs-notes)