# Virtual Private Cloud

	Author	: Tim Nikolajsen
	Date	: 2018-11-01
	Purpose	: Keep a guide to launch VPCs.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-11-01 | Initial Document                              | Tim Nikolajsen |

___

***The all happens under 'Service', 'VPC'.***

# VPC

## Creating a VPC
- Use the 'Launch VPC Wizard' button
	- Select CIDR block
	- Pick Availability Zone
	- Create the correct security group

## Creating VPC with both private and public subnets
- In the wizard select VPC with private and public subnets on the left.
	- Follow the same steps when creating a single subnet but set settings for both public and private.
	- Choose either to use NAT gateway or NAT instance.
	
[Additional Notes](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_44_VPC.md)