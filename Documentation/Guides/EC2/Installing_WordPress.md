# WordPress Amazon Linux 2

	Author	: Tim Nikolajsen
	Date	: 2018-10-04
	Purpose	: Keep a guide to install a WordPress server on Amazon Linux 2.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-04 | Initial Document                              | Tim Nikolajsen |

___

# Install WordPress on Linux
- Follow Amazon's own [guide](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html) (Their own step-by-step guide)

Additional notes about can be found [here](https://gitlab.com/Azarion/ss2-aws/blob/master/Notes/Week_40_EC2.md)