VPC (Virtual Private Cloud) is a service that enables the creation of a network in the same way as a network engineer would create a physical network with subnets, end devices, security, etc.

VPC subnets have two different types when setting them up:
- Public subnets which are accessible from the internet and are given a Public IP address.
- The private subnet is not given a public IP and therefore not reachable from the internet.
- For each subnet to be able to communicate with each other a NAT service is required, whether it is Amazons own NAT Gateway or a NAT Instance which is just an instance (or VM) running a NAT service.

End devices are any old device in the same sense as physical end devices such as hosts, network storage etc.
Security settings are split into two methods of security: ACLs (Access Control Lists) and Security Groups.
- ACLs are used to control the access to buckets and objects and is done with whitelisting instead of blacklisting.
- Security Groups are a set of controls which act as a kind of firewall between the VPC and whatever it is connected to.

A major difference in doing these things in Amazons virtual environment is setting up a subnet or two takes less than 5 minutes while manually inserting every setting whereas it will take around at least 15min to implement in a physical environment.
All of this not just doable from Amazons web client but also via a command line interface which enables for automation to speed up creating a network even faster.
