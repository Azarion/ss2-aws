# Week 38 Notes

	Author	: Tim Nikolajsen
	Date	: 27/9/18
	Purpose	: Keep list of notes made from week 38 during the Special Subject.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 27/09/2018 | Initial Document                              | Tim Nikolajsen |


## Account Creation Notes

#### Credit Card information

- Credit Card information is required because of the business models Amazon uses where you get ~750 hours of free VM time and when you go past this limit then you have to pay.

#### Amazon will reserve $1 from you bank account

- Amazon will reserve $1 on your credit card to verify that the information is correct and not fraudulent.

#### Phone Call

- You will be called by a robot to verify the account. (Free)