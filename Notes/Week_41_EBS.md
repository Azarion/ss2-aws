# Week 41 Notes

	Author	: Tim Nikolajsen
	Date	: 2018-10-11
	Purpose	: Keep list of notes made from week 41 during the Special Subject.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| :------: | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-11 | Initial Document                              | Tim Nikolajsen |
| 2.0      | 2018-10-12 | Added notes about EBS                         | Tim Nikolajsen |


## EBS Notes

### EBS Volumes during instance termination

- Nothing will happen to the EBS volume when the attached intance is terminated.

#### Notes for Volumes during instance termination

- EBS Volumes does not terminate when the attached instance is terminated because EBS volumes are persistent storage and you pay $0.10/gigabyte/month (you pay 10 cents for running a 1GB storage devices for a whole month)
	- You can however set the volume to delete upo instance termination.

### EBS Snapshots

- Problem
 - Recovered volume not in the same availability zone

#### Notes for EBS Snapshots

- When you recreate a volume using snapshots you need to specify where the volume is located.
	- This is because you are simply making a new volume which includes the settings from the snapshot and not directly recreate the old volume.
	- This allows for an admin to upgrade the size of the storage without losing any settings. (The elasticity part of EBS)