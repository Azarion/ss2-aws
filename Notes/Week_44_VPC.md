# Week 44 Notes

	Author	: Tim Nikolajsen
	Date	: 2018-11-01
	Purpose	: Keep list of notes made from week 44 during the Special Subject.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| :------: | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-11-01 | Initial Document                              | Tim Nikolajsen |


## VPC

### Private and public subnets

- When creating both public and private subnets you need a NAT.
	- This can be either a NAT Instance or a NAT gateway.
		- The NAT gateway is not free
		- The NAT instance is free but as an instance it is depleting the monthly 750 hours of free time to run instances.