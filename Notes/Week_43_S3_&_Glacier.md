# Week 43 Notes

	Author	: Tim Nikolajsen
	Date	: 2018-10-25
	Purpose	: Keep list of notes made from week 43 during the Special Subject.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| :------: | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 2018-10-11 | Initial Document                              | Tim Nikolajsen |


## S3/Glacier Notes

### S3 Cannot recover deleted objects.

- When deleting an object it takes forever to get the "Delete Marker". And when it finally shows up the option to "Undo Delete" is greyed out and not clickable.

### Solution

- No Solution