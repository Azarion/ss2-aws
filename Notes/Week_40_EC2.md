# Week 39 Notes

	Author	: Tim Nikolajsen
	Date	: 05/10/18
	Purpose	: Keep list of notes made from week 39 during the Special Subject.
	Audience: Myself


## Revision

| Revision | Date       | Comments                                      | Changes By  	 |
| -------- | ---------- | --------------------------------------------- | -------------- |
| 1.0      | 05/10/2018 | Initial Document                              | Tim Nikolajsen |

___

### Installing WordPress

How to install WordPress on Amazon Linux depends on what image you are using of Amazon Linux.

[How-to-Install on Amazon Linux 2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-lamp-amazon-linux-2.html)

### Notes

Apparently there is enough of a difference between Amazon's Linux images that they have multiple guides for whichever version you choose to create.

___

### Password for Windows Server Image

To get your administrator password, when an Instance of Windows Server has been created, you need to wait a couple of minutes before you can get your password.

To get your password you need to point AWS to the Private key file of the Key Pair to get a readable password you can use to log in.

### Notes

____

